<?php

class Dashboard {
  public $details;

  private $resources;
  private $submissions;
  private $fields;
  private $wrapper;
  private $resourceKeys = [
      'init',
      'views',
      'submissions'
  ];

  public function __construct($wrapper, $resources) {
    $this->wrapper = $wrapper;
    $this->resources = $resources;
  }

  private function getDetails() {
    if (!$this->details) {
      $this->details = $this->wrapper->getFormDetails(FORM_ID);
    }
    return $this->details;
  }
  private function formatNumber($number) {
    if ($number == 1) {
      return 'once';
    }  else {
      return $number . ' <span>times</span>';
    }
  }
  private function getFieldLabels() {
    $fields = $this->getDetails()->fields;
    $return = [];
    foreach ($fields as $field) {
      $return[$field->id] = $field->label;
    }
    return $return;
  }
  private function setResource($key, $value) {
    $this->resources[$key] = array(
      'content' => $value
    );
    return $this->getResource($key);
  }

  public function getResourceKeys() {
    return $this->resourceKeys;
  }
  public function getResource($key) {
    if ($key === 'submissions' || $key === 'views') {
      if ($this->getDetails()->$key) {
        $res = $this->formatNumber( $this->getDetails()->$key );
      } else {
        die('API limit exceeded');
      }
    } else if ($key === 'init') {
      // build init resource
      $init = new stdClass;
      $init->url = $this->getDetails()->url;
      $init->fieldLabels = $this->getFieldLabels();
      if (!isset($this->resources[$key])) {
        $this->setResource($key, $init);
      }
      $res = $this->resources[$key]['content'];
    }
    return $res;
  }
  public function getSubmissions() {
    if (!$this->submissions) {
      $submissions = array();
      foreach ($this->wrapper->getSubmissions(FORM_ID) as $sub) {
        $submission = $this->wrapper->getSubmissionDetails($sub->id);

        // just so i dont have to do this on the front end
        $submissions_data = array();
        foreach($submission->data as $data) {
          $data->value = nl2br($data->value);
          $submissions_data[] = $data;
        }
        $submission->data = $submissions_data;

        $submissions[] = $submission;
      }
      $this->submissions = $submissions;
    }
    return $this->submissions;
  }
  public function getField($id) {
    if (!isset($this->fields[$id])) {
      $field = json_decode($this->wrapper->request('field/' . $id . '.json'));
      $this->fields[$id] = $field->label;
    }
    return $this->fields[$id];
  }
  public function deleteSubmission($id) {
    $status = json_decode($this->wrapper->request('submission/' . $id . '.json','DELETE'));
    return $status->success;
  }

}
