function pulse($el) {
  $el.addClass('pulse');
  setTimeout(function(){
    $el.removeClass('pulse');
  },500);
}
var Dashboard = function(){
  return {
    $formWrapper: $('#form-wrap'),
    $formHeading: $('#stats-wrap'),
    $submissions: $('#submissions'),
    $viewFormBtn: $('#view-form'),
    initData: {},
    fieldLabels: {},
    loaded: 0,
    toLoad: 3,
    reloadable: [],
    refreshData: function(res) {
      var $val = this.$formHeading.find('span[data-resource="' + res.id + '"]');
      var oldContent = $val.html();
      if (oldContent !== res.content) {
        $val.html(res.content).parent().removeClass('h');
        pulse($val.parent());
        if (res.id === 'submissions') {
          // trigger fetch new submissions if number has changed
          // work out exactly how much that difference is
          var offset = parseInt(oldContent) - parseInt(res.content);
          if (offset > 1) {
            // dont trigger on delete
            this.fetchNewSubmissions(offset);
          }
        } else {
          this.moduleLoaded();
        }
      }
    },
    getFieldLabel: function(id) {
      // console.log(id);
      var field = this.fieldLabels[id];
      if (field !== undefined) {
        return '<p>' + field + '</p>';
      } else {
        // resource isn't ready yet, just print a placeholder
        // with data attribute, it'll get filled in later
        return '<span class="placeholder placeholder-fieldlabel" data-field-id="' + id + '"></span>';
      }
    },
    deleteSubmission: function($sub) {
      var t = this;
      var id = $sub.data('sub-id');
      $.ajax('/api/resource.php?delete=' + id, { dataType: 'json' })
      .done(function(data){
        if (data.content === '1') {
          $sub.addClass('h');
          setTimeout(function(){
            $sub.remove();
            t.refreshData({
              id: 'submissions',
              content: (parseInt(t.$formHeading.find('span[data-resource="submissions"]')) - 1)
            });
          });
        } else {
          alert('delete failed');
        }
      })
      .fail(function(jqXHR) {
          t.failedAjax(jqXHR);
      });
    },
    fetchNewSubmissions: function(offset, reverseOrder) {
      var t = this;
      $.ajax('/api/resource.php?submissions', { dataType: 'json' })
      .done(function(data) {
        // only grab the offset # off the top and prepend it to the wrapper
        $.each(data.content.splice(0,offset),function(i,sub){
          var $newSubmission = $('<div class="well-wrap h" id="submission-' + i + '" data-sub-id="' + sub.id + '"><div class="well">\
            <a href="#" class="btn-close">&times;</a>\
          </div></div>');
          $.each(sub.data,function(ii,field){
            var $newField = $('<div class="h row-wrap"><div class="row">\
            <div class="col-sm-3 text-right field-label-wrap">' + t.getFieldLabel(field.field) + '</div>\
            <div class="col-sm-9">' + field.value + '</div>\
            </div></div>');
            $newSubmission.children().append($newField);
            setTimeout(function(){
              $newField.removeClass('h');
            }, 250);
          });
          // if this is the first call, make sure to order submissions correctly
          if (reverseOrder !== undefined) {
            t.$submissions.append($newSubmission);
          } else {
            t.$submissions.prepend($newSubmission);
          }

          setTimeout(function(){
            $newSubmission.removeClass('h');
          }, 250);
        });
        t.moduleLoaded();
      })
      .fail(function(jqXHR) {
          t.failedAjax(jqXHR);
      });
    },
    startLiveReload: function() {
      var t = this;
      $.each(reloadable,function(i,el){
        reloadQueue[el] = {
          timer: setInterval(function(){
            if (!reloadQueue[el].running) {
              // console.log('refreshing ' + el);
              reloadQueue[el].running = true;
              $.ajax('/api/resource.php?id=' + el, { dataType: 'json' })
              .done(function(data) {
                t.refreshData(data);
                t.moduleLoaded();
                reloadQueue[el].running = false;
              })
              .fail(function(jqXHR) {
                  t.failedAjax(jqXHR);
              });
            }
          }, 20000),
          running: false
        };
      });
    },
    moduleLoaded: function() {
      // called every time a module is done loading over ajax
      // - used to determine when app is ready
      this.loaded++;
      if (this.loaded === this.toLoad) {
        $('#loading').addClass('h');
        $('.stats,#submissions-wrap').removeClass('faded');
      } else {
        $('#loading .progress-bar').css('width', this.loaded * (100 / this.toLoad) + '%');
      }
    },
    failedAjax: function(jqXHR) {
      var data;
      try {
          data = $.parseJSON(jqXHR.responseText);
      }
      catch (e) {
          data = {
              description: 'Server is unreachable'
          };
      }
      $container.append('<div class="alert alert-danger">' + data.description + '</div>');
    },
    init: function($container) {
      var t = this;

      // get initial submissions
      t.fetchNewSubmissions(3, true);

      // Get all resources
      $.ajax('/api/resource.php', { dataType: 'json' })
      .done(function(data) {
          data.resources.forEach(function(resource,i) {
              $.ajax(resource.href, { dataType: 'json' })
              .done(function(resourceData) {
                  t.$formHeading.find('.resource-loading').addClass('h');
                  if (reloadable.indexOf(resourceData.id) !== -1) {
                    t.refreshData(resourceData);
                  } else {
                    if (resourceData.id === 'init') {
                      t.initData = resourceData.content;
                      t.$viewFormBtn.data('form-url',t.initData.url).removeClass('disabled');
                      t.fieldLabels = t.initData.fieldLabels;
                      // fill in fieldlabels that need to be filled
                      var $labelPlaceholders = $('.placeholder-fieldlabel');
                      if ($labelPlaceholders.length > 0) {
                        $labelPlaceholders.each(function(ii,el){
                          var $el = $(el);
                          var fieldId = $el.attr('data-field-id');
                          var $newLabel = $('<p class="h">' + t.fieldLabels[ fieldId ] + '</p>');
                          $el.addClass('h').after($newLabel);
                          setTimeout(function(){
                            $newLabel.removeClass('h');
                          },250);
                        });
                      }

                      $('#formModal').on('shown.bs.modal', function () {
                        $('#formModal .embed-responsive').html('<iframe class="embed-responsive-item" src="' + t.$viewFormBtn.data('form-url') + '"></iframe>');
                      });
                      t.moduleLoaded();
                    }
                  }
              })
              .fail(function(jqXHR) {
                  t.failedAjax(jqXHR);
              });
          });

          // start getting reloadable resources
          t.startLiveReload();
      })
      .fail(function(jqXHR) {
          t.failedAjax(jqXHR);
      });

      // bind events
      $('body').on('click','.btn-close',function(e){
        e.preventDefault();
        var q = confirm('Are you sure you want to delete this submission?');
        if (q) {
          t.deleteSubmission($(this).parent().parent());
        }
      });
    }
  };
};

var app = new Dashboard();
var reloadable = [
  'submissions',
  'views'
];
var reloadQueue = {};
var $container;

$(function() {
    'use strict';

    $container = $('[data-resource-container]');

    app.init($container);

});
