Formstack Dashboard
===================

Instructions
1. `npm install`
2. `bower install`
3. `grunt serve`

Open http://localhost:9000/ in your browser to view the local version

To build for production, type `grunt build` instead of `grunt serve`